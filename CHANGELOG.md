# Changelog

## 0.2.0

- Add scripts from Home assistant addons example

## 0.1.0

- Initial build
