#!/usr/bin/with-contenv bashio
# shellcheck shell=bash

main() {
    bashio::log.trace "${FUNCNAME[0]}"

    while true; do
        echo "Second Script Output"
        sleep 10
    done
}
main "$@"
